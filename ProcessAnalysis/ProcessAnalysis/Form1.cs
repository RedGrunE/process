﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;

namespace ProcessAnalysis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ListOfProcess = Process.GetProcesses().OrderBy(x => x.Id).ToList();
            listBox1.Items.Clear();
            foreach (var item in ListOfProcess)
            {
                string CountOfTab = "\t\t"; 
                listBox1.Items.Add("ID: " + item.Id + " " + CountOfTab + " Name: " + item.ProcessName);
            }
        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            listBox2.Items.Clear();
            try
            {
                Process myProc = Process.GetProcessById(int.Parse(listBox1.SelectedItem.ToString().Split(' ')[1]));
                listBox2.Items.Add("Потоки процесса: " + myProc.ProcessName);
                ProcessThreadCollection threads = myProc.Threads;
                foreach (ProcessThread pt in threads)
                    listBox2.Items.Add("Thread ID: " + pt.Id + "\tВремя: " + pt.StartTime.ToShortTimeString() + "\tПриоритет: " + pt.PriorityLevel);                
            }
            catch (Exception ex)
            {
                listBox2.Items.Add("Отказано в доступе");
            }
        }
    }
}
